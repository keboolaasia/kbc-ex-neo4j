from neo4j.v1 import GraphDatabase, Record
from exneo4j.extractor import Extractor
import pytest
import types
import csv
import os
from exneo4j.kbc import main

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

URI ="bolt://db:7687"
AUTH = ("neo4j", "keboola")

@pytest.fixture
def driver():
    driver = GraphDatabase.driver(uri=URI, auth=AUTH)
    return driver

@pytest.fixture
def ex():
    return Extractor(uri=URI, auth=AUTH)


def test_can_connect(driver):
    """Make sure the database is up and running"""
    with driver.session() as session:
        with session.begin_transaction() as tx:
            for record in tx.run("MATCH (a:Master)"
                                 "WHERE a.firstname = {name} "
                                 "RETURN a.lastname", name="John"):
                assert record["a.lastname"] == "Walsh"


def test_extractor_returns_Record_object(ex):
    simple_query = ("MATCH (person:Master)-[:PURCHASED]-(product:Product) "
                    "WHERE person.firstname='John' "
                    "RETURN person.firstname, person.lastname, product.title")
    results = ex._query(simple_query)
    assert isinstance(results, types.GeneratorType)

    for rec in results:
        assert isinstance(rec, Record)

def test_serializing_simple_record_to_json(ex):
    """
    Test

    A simple query is a query which specifies return values in "node.property"
    format

    """

    rec = Record(keys=('person.firstname', 'person.lastname', 'product.title'),
                 values=('John', 'Walsh', 'Motor'))
    serialized = ex._record_to_json(rec)
    expected = {'person.firstname': 'John',
                'person.lastname': 'Walsh',
                'product.title': 'Motor'}

    assert expected == serialized

def test_query_saves_to_csv(ex, tmpdir):
    simple_query = ("MATCH (person:Master)-[:PURCHASED]-(product:Product) "
                    "WHERE person.firstname='John' "
                    "RETURN person.firstname, person.lastname, product.title")
    outpath = tmpdir.join('flattened.csv')
    outpath = ex.query_to_csv(outpath.strpath, simple_query)

    expected_row = {'person.firstname': 'John',
                'person.lastname': 'Walsh',
                'product.title': 'Motor'}

    with open(outpath) as f:
        reader = csv.DictReader(f)
        lines = [l for l in reader]
        assert expected_row in lines

def test_functional_extractor():
    tables = main(os.path.join(THIS_DIR, 'data'))
    assert os.stat(tables[0]).st_size > 1
