#!/bin/ash
bin/neo4j start

echo "database is ready @ ${DB_PATH}. Filling with data"
sleep 2

bin/cypher-shell -u neo4j -p neo4j < /src/tests/data/test_insert_query.cql

bin/neo4j stop
