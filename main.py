from exneo4j.kbc import main
import sys
import os
import traceback

if __name__ == "__main__":
    try:
        main(datadir=os.getenv('KBC_DATADIR'))
    except (ValueError, KeyError) as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    except Exception as err:
        print(err, file=sys.stderr)
        traceback.print_exc(file=sys.stderr)
        sys.exit(2)
        raise
