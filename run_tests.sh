#!/bin/ash

echo "Checking if neo4j is running"
end="$((SECONDS+10))"


# while true; do
#     [ "200" = "$(curl --silent --write-out %{http_code} --output /dev/null http://db:7474)" ] && break
#     sleep 1
# done

echo "Running tests"
cat <<EOF | python3 && cd /src && /usr/bin/python3 -m 'pytest'
import requests, time
def is_up():
    try:
        status = requests.get('http://db:7474').status_code == 200
    except requests.ConnectionError:
        status = False
    finally:
        return status
while is_up() is False:
    print("Neo4j not up yet!")
    time.sleep(2)
print("Neo4j is up")
EOF

