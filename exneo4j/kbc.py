from keboola import docker
from exneo4j import Extractor
import os


def main(datadir):
    print("Startin extractor")
    cfg = docker.Config(datadir)
    params = cfg.get_parameters()
    uri = 'bolt://{database_ip}:{database_port}'.format(**params)
    print("Connecting to: ", uri)
    auth = (params['username'], params['#password'])
    ex = Extractor(uri, auth=auth)
    queries = params.get("queries", [])
    tables = []
    for query in queries:
        outpath = os.path.join(datadir,
                               'out/tables',
                               query['tablename'] + '.csv')
        print("Executing query", query['query'])
        print("Downloading to", outpath)
        ex.query_to_csv(outpath, query['query'])
        print("OK")
        tables.append(outpath)

    return tables
