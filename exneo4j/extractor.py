"""
@author: robin@keboola.com

"""
import csv
from neo4j.v1 import GraphDatabase


class Extractor:
    def __init__(self, uri, auth):
        self._driver = GraphDatabase.driver(uri, auth=auth)

    def _query(self, q, params=None):
        if params is None:
            params = {}
        with self._driver.session() as session:
            with session.begin_transaction() as tx:
                for record in tx.run(q, **params):
                    yield record

    @staticmethod
    def _record_to_json(record):
        """
        Convert one record (possibly nested) to flat dict

        """
        return record.data()

    def query_to_json(self, q, params=None):
        """
        Fetch a query and serialize into flat csv structure of dicts

        This can be used in csv.DictWriter

        """
        for raw_record in self._query(q, params):
            yield self._record_to_json(raw_record)

    def query_to_csv(self, outpath, q, params=None):
        """
        Serialize query to csv'

        """
        with open(outpath, 'w') as f:
            rows = self.query_to_json(q, params)
            try:
                first_row = next(rows)
            except StopIteration:
                print("Query '{}' did not return any results".format(q))
                outpath = None
            else:
                writer = csv.DictWriter(f, fieldnames=list(first_row.keys()))
                writer.writeheader()
                writer.writerow(first_row)
                for row in rows:
                    writer.writerow(row)
        return outpath
