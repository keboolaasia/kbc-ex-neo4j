VERSION=0.1.0
IMAGE=pocin/kbc-ex-neo4j
DBIMAGE=neo4j:3.3
TESTCOMMAND=docker-compose up
test:
	eval $(TESTCOMMAND)

run:
	docker run --rm -v `pwd`:/src/ -e KBC_DATADIR=/src/tests/data/ ${IMAGE}:latest

sh:
	docker run --rm -it --entrypoint "/bin/ash" -v `pwd`:/src/ -p 17474:7474 -p 17687:7687 -e KBC_DATADIR=/src/tests/data/ ${IMAGE}:latest

build:
	echo "Building ${IMAGE}:${VERSION}"
	docker build -t ${IMAGE}:${VERSION} -t ${IMAGE}:latest .

deploy:
	echo "Pusing to dockerhub"
	docker push ${IMAGE}:${VERSION}
	docker push ${IMAGE}:latest

db:
	docker run --rm -it -v `pwd`/tests/data/graph.db:/data/ -p 7474:7474 -p 7687:7687 ${DBIMAGE} 

dbsh:
	docker run --rm -it -v `pwd`/tests/data/graph.db:/data/ -v `pwd`:/src -p 7474:7474 -p 7687:7687 ${DBIMAGE} /bin/ash

interactive:
	docker-compose -f ./docker-compose.interactive.yml up
