FROM frolvlad/alpine-python3:latest

MAINTAINER robin@keboola.com

RUN pip install --upgrade --no-cache-dir --ignore-installed \
    requests \
    pytest \
    neo4j-driver \
    && pip install --upgrade --no-cache-dir --ignore-installed https://github.com/keboola/python-docker-application/archive/2.0.0.zip

COPY . /src/

CMD python3 -u /src/main.py
# prepare the container
WORKDIR /home
